There are three belief structures, and three economic structures at play in the mind of a [[Mutual Aid]] or open source participant.
### Belief Structures
This is the level of fanaticism of the individual.
1. The Devout. "Mutual Aid/Open Source (MA/OS) is my life! I only exist to help others and maintain these structures, to give in what I can and take only what I need."
2. The Moderate. "MA/OS is a Good Thing. I am willing to spend some of my time and effort to help it."
3. The Opportunist. "MA/OS is alright. I use it when I need, and I respect the people who put in all that effort into it."
### Economic Structures
This is the level of anti-commercialism of the individual.
4. The Devout. "Charity is nothing but theft through guilt and PR. People need to put themselves into the system in order to have a system. You can't throw money at something and expect someone else to do it all."
5. The Moderate. "Large-scale commercial aid organizations, like Red Cross, have so much overhead that donating money only pays some middle manager's salary. Local donations, however, help neighbors in need."
6. The Opportunist. "Charity, Mutual Aid, whatever, as long as needs are met. We aren't supposed to care who did the work, right?"
### Together
Each person taking part in Mutual Aid, just as in Open Source, will be a combination of 1-3 and 4-6.