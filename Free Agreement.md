Free Agereement is the basis of anarchist communism, as opposed to the state ocmmunism of Marx. Benjamin Tucker and [[Kropotkin]] both agree that Marxism is a form of state communism. 

Anarchists do not want to cede control to the state.  For a person to give over control to a ruling elite is no different in a Marxist-Leninist society than it is in a capitalist one.

Tucker believed that the relatoinship of of communism was destined to be a statist, Jacobin model communism. Kropotkin believed anarcho communism is possible, and that is because anarcho communism is based on free agreement.
# Free Agreement is not a contract.
"Contract" here means a contract from an individual to the society rather than between two individuals.
## Free agreement has three components.
### 1. Contracts are static with fixed provisions and assume the parties have equal status and capability of fulfillment.
Free agreements change during the agreement. The change is driven by the constant adjustment and re-adjustment of social forces, forces which are unequal, complex, and diverse.
### 2. Free agreement does not allow for consent based on submission to law, and to those defending on obedience to authority.
The first part includes religious rites and marriage as well as taxation. THese are based on *constitutionally* guaranteed rights to exclusive ownership.
### 3. Free agreement is based on individual judgment.
Law is based in fear as well as security. Free agreement has no servility in it because everyone **-each person-** decides what they feel is right. Here [[Kropotkin]] shows that it is more than "rules not rulers"; rulers are bad, as are rules based on coimpliance with other people's standards.

# Free Agreement is individual power.
Free agreement gives power to the individual by preventing economic domination (ha) and slavish compliance with prevailing norms (ha ha).

Contracts structure economic inequality and enforce obedience.