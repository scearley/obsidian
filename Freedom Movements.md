All movements of freedom are about people's rights. 

All movements of freedom must include recognition that people must be able to feed themselves. The movement must not ensure enough food exists, it must not ensure that the State will supply food to the people. It is about people having the security to fulfill their own needs. This is not about growth, it is about meeting needs.

It is not a question of "that's all it is", but rather "why should it be anything more than a basic meeting of needs"?

A movement is not about a right to work (cf. [[Right to Work is not correct thinking. Right to well being is.]]). It is about the assurance of having needs met, starting with the basics of food and shelter.

An individual's place within a movement is not about status, as this leads to hoarding, either of materials or status (cf [[Drive of Ownership in Open Source]], [[Hypocrisy in church leadership]])