Expropriation is necessary to have equality.

Expropriation will not happen through legislation. Legislators are the ruling elite. (cf [[Authoritarianism rising in socialism]])

This can only happen through revolution. Legislation cannot ensure food safety (cf [[Revolutionary failures]]), food safety can only happen by ensuring food safety. You must seize control of the granaries, you must seize control of empty houses, you must seize control of medical centers. Then what do you do?
Work?
**No.**

You (the community) feed the hungry, shelter the homeless, care for the sick. 

People do not have a right to work, and will not feel free if the revolution first grants everyone a job. People have a right to well being, they deserve the social wealth produced by previous and current generations. (cf [[Right to Work is not correct thinking. Right to well being is.]]) They have a right to determine what the life of comfort is. Not a law. Not a leadership. That person, only, and that ability to determine is only realized through a community ensuring all needs for that life of comfort will be met.

idea based on reading ch 2 of [[Conquest of Bread, The]]