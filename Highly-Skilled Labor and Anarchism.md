What about the jobs that require years and years of a specialized skill, such as architectual engineering, or surgery?

Jobs that require so much skill, but similarly does not mean you should be rewarded monetarily for an advanced skill. All "advanced" skills are equal, all skills are equal. (cf. [[Craftsmanship and Communal Labor]])

The reqard for pursuing and having that high level of skill is getting to devote yourself to that skill, to only have to concern yourself to that skill. **Money is not the** ***only*** **motivator.**
