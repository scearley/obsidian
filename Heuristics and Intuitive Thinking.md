# Heuristics

The reliance on a heuristic results in predictable bias in judgment. This is a bias in intuitive thinking, and affects all sorts of desicions using assigned probabilities - forecasting the future, assessing hypotheses, and estimating frequencies.

*Are people as good at intuiting grammar as they are statistics?*
We already know people are good at intuiting grammar. Any four year old can figure out and conform to the rules of grammar even though they don't know what the rules are. Do people do the same with statistics? 

## Heuristics as a simplifying shortcut

We have subjective biases. We are **way** too willing to accept and believe research findings based on inadequate evidence, and when we do our own research we tend towards not collecting enough data.

We have at least 20 different identified biases in our judgment methods ("Judgment under Uncertainty: Heuristics and Biases" in Kahneman's *Thinking, Fast and Slow*).

Why do we havge these biases? They do prove useful. They allow for quick decision making. They are regularly correct (such as in the ubiquitous rule of thumb). You can tell from the first word whether someone on a phone call is angry.

# Systemic errors

But these can still lead to severe, and systematic, errors. And this is because we let our "gut reactions" affect our logic. (cf [[Two system approach of thinking]])