# What is attentive thinking
Attentive thinking is the conscious, cognitive process we use to create solutions. They have two main features:
1. They require attention
2. They are disrupted when attention is shifted
## Many variations
Unlike [[Automatic Thinking]], Attentive thinking requires many different kinds of operations. 
- Monitoring the appropriateness of your behavior in public
- Counting the number of times the letter ***i*** appears on a page
- Comparing two appliances
- Parking in a small space
- Focusing on the voice of one person in a crowded room

In each one of these cases you have to pay attention and if you do not you do the task less well, or even detrimentally. It's easy to distract your attention from completing any of these tasks. 

## Attention has limited capacity
"Pay attention" is a precise phrase: you have only a limited budget of attention you can use at one time, and if you exceed your budget, you will fail.

Actions that require a lot of effort will interfere with each other. You can do several things at once but only when they're easy and undemanding.

The intense focus required of some tasks will overwhelm to the point where other outside stimuli are ignored.

