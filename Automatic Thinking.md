# What is automatic thinking
Automatic thinking is an unconscious cognitive process we use for a fast solution. 
For example, symbol processing. Reading. 

Once we learn how to read, it is **impossible** for us to see a word and not read it. Once we learn symbol processing we automatically process those symbols when we encounter them again.

Consider not only words, but the symbols you read while out for a hike. The symbols of the surrounding area that point to where the trail is. You automatically know when there is a fork. The symbols of dirt and growth all tell you "these are two paths now, and I need to pick one".

If I show this:
$$ 17 \times 24 $$
then the immediate result is to see it has to do with math, and it is multiplication. There's also a vague sense of realistic answers - you know immediately it's not 12,588, for instance, and also not 99. But if I said it were 378 you might pause. You don't have a precise solution. 

And if I asked you to solve it you would go through a number of steps, including
- Remembering how to multiply
- implementing how to multiply
- feel the burden of carrying so much added material in your brain, keeping track of all of the middle placements
- it also involves your body:
	- tensed muscles
	- higher blood pressure
	- increased heart rate
	- dialated pupils
The mental work is the system 2 thinking, the slower, deliberate, effortful, orderly way of thinking. Slow thinking. And this is how we see that automatic thinking is the component of slow thinking.

The answer is $408$, by the way.


Other examples of automatic thinking include
- Determining which object is closer than another
- Turning towards the apparent source of a sudden sound
- Making a "disgusted" face when shown a horrible picture
- Detecting hostility in a voice
- 2+2
- recognizing the phrase "a passion for detail" is a job stereotype

## More than a subconscious process
Automatic thinking includes what we refer to as autonomic nerve impulses, like automatic motor skills, heartbeat, breathing. But on a not-that-deep level, it includes implicit biases, fast solutions, and "gut feelings".

Automatic thinking is the "System 1" process, and this is in contrast to - but always works in combination with - our controlled, explicit, methodical thought process known as System 2 thinking.  System 2 thinking requires active focus and is easily disrupted by distractions.

## Influence
Automatic thinking influences us regarding things we have no control over, and we don't know we are reacting to it.

This leads us into cognitive shortcuts and biases. Because System 1, automatic thinking is always affecting System 2, [[Attentive Thinking]] no matter how much we try to shut it out, we have to keep extra vigilant about removing biases and errors in our evaluations.

The question is, can you identify when it is happening, and how much can you reduce the affect of System 1 thinking on System 2?

This leads into [[nudging]].