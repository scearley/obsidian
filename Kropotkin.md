# Peter Kropotkin
also spelled *Petr*

Was part of the old Russian nobility, then after army service realized his anarchism. Jailed in 1874 he escaped (it was a low-security prison) and lived in exile for 41 years and returned to Russia after the Russian Revolution in 1917.

He was a proponent of the decentralized communist society, based on voluntary associations of self-governing communities.

