The system of no oppression is the system that obliges us to have no government.
Capitalist systems require the government to regulate the economy.
Socialist systems require the government to regulate production. Socialist systems also require government to ensure nobody is taking advantage of another person's work.
Under communism (not Marxism), people work to produce what they think is needed by society, and people freely take what they need. (cf [[Drive of Ownership in Open Source]] and [[Work, output, and freedom]])

