The theory has two parts.
# Layout
A design is either
- Horizontal
- Vertical
- Diagonal
(can a design be circular?)

# Meaning
Each direction has underlying meanings
### Horizontal Layouts
Horizontal layouts are more calming and stable.
### Vertical Layouts
Vertical layouts convey balance, boldness, and alertness. THey are impactful.
### Diagonal Layouts
Diagonal layouts invoke movement and action.

