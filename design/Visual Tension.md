Visual Tension is graphically designing something in a way that forces the viewer to experience, to take in the design in a disruptive way (or ways).

An examplke is creating designs that violate the harmony of the [[Structural Net Theory]].

However, this rarely works.