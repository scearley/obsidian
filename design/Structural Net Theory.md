Structural Net Theory was proposed by Rudolph Arnheim.

> 1. All canvases have the same structure.
> 2. This structure exists before anything is placed on the canvas.

## Description of the canvas
- The center of the canvas is the focus.
  This focus is the attraction center.
  This center is not the "geographical" center but a spot slightly above the canvas center, where the eye naturally comes to rest.
- There is a square (or rectangle) made with each corner halfway between the center and each corner of the canvas.
  These corners of the square are also attraction centers.
  Each attraction center will have a circular area around it defining the attraction center. It is not a cingle point but an area.

So far this results in the center, with its radius, and the square (rectangle) with its four radii. Five attraction centers so far.

- There are axes made diagonally, vertically, horizontally, creating an eight-arm asterisk (___*___) centered on the focal center of the canvas, the midpoints of the sides of the square and canvas, and the corners of the square and canvas.
- These axes are Visual attractors, determining and even forcing eye flow through the design.
## Questions about the theory
The theory thus says you shoudl design to these focii. But it also assumes the canvas size is set before the image is set. Would this mean that if I come up with a design that is "unbalanced" that I could possibly balance it by resizing the canvas and merely shifting the design on the canvas?

It seems obvious the answer is **yes**. 

_And why not?_  It makes sense to shift the primary around on a larger background.
