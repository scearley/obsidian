There is a substantive problem in modern socialism. The founders of this modern socialism (namely Charles Fourier, Henri de Saint-Simon, and Robert Owen) felt that they were the founders of a new religion.

SInce those "big three" considered themselves _de facto_ popes or cult leaders, they had to control the movement step, by step, by step. (cf. [[Expropriation, legislation, revolution, and anarchy]])

They lived through the French Revolution, and saw all of the mistakes and failures of the revolution. They chose not to believe in the proletariat doing the necessary work and instead believed in some kind of Socialist Savior, a Napoleonic figure.

The necessary structure of top-down in these socialist situations are only there to preserve an elite. Those who previously were in control refuse to share.

From preface to [[Conquest of Bread, The]].