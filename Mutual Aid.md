## Meaning
Mutual aid is an organizational theory that voluntary reciprocal exchange of resources for mutual benefit is the idea social structure across groups. This can apply in many areas (see below), but here this page is specifically for the ideas of mutual aid as commonly read as a way of working and distributing to fill needs and have needs filled.

## Background
Mutual Aid was a term popularized by [[Kropotkin]] to describe a process of evolution through cooperation instead of competition. He argued it was promoted by natural selection and drives evolution. It was the anithesis to social Darwinism, and refuted Rousseau's belief that cooperation was motivated by idealized (Platonic) love.

## How it works
Each group has members who organize themselves within the group, and then with other groups. This is where [[Federation]] comes in to unite the supply chains. There is no hierarchy, no bureaucracy, and the members as a whole control all resources. Within each group the structure is typically (but not necessarily) egalitarian, running on participatory democracy, and consensus-based decision making. When mutual aid movements encompass more than one group, such as in a federated Mutual Aid network, then it is not necessary that each group be run this way. However, the [[Federation (mutual aid)]] itself *must* be run in that way.

## In Practice
Most commonly mutual aid is seen providing people with food, medical care, and disaster relief.
Mutual aid is not charity. It is the collective solidarity of giving what you can and getting what you need. 

The Black Panthers created many mutual aid movements, most especially Breakfast for Children.

## Common failure points
Generally, mutual aid fails for one or more of the following reasons:
+ Lack of funding
+ Lack of technical experts
+ Lack of public support (legitimization)
+ Lack of staff, especially during periods where aid is not active
+ Government interference, either indirect through not giving grants, or direct such as police destroying distribution points
+ Burnout of the people involved
+ A de-evolution into social hierarchies, which mayh not necessarily result in the loss of aid to those who need it, but it will result in the loss of "give what you can get what you need" principles. Social hierarchies often lead to discrimination as well.

Compare these to the [[Revolutionary failures]]

### Other similar spaces
+ [[Federation (machine learning)]]
+ [[Federation (software)]]
+ [[Federation (mind)]]
