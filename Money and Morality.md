***Money destroys our ability to live up to our morals***
Consider your child comes home from school without a jacket, and explains he gave it to the kid who didn't have a winter coat.
Your reaction is likely in two parts:
1. "I'm so proud of you, you are such a caring, wonderful person"
   but that quickly becomes
2. "But that was your new winter coat! You should take your old coat and give it to them. Or we could even go get them a new, cheaper coat."

We don't give to people because money is a precious, finite resource. Money is limited, resources are scarce, so we *have* to be competitive because of society.
Society teaches us, because of this hypercompetitivism, that we have to look out for ourselves first. If we look out for other people we will be shut out of these so-called limited resources and people will take advantage of you, and even exploit you. (cf. [[Prestige and Work]], [[Culture of Gifting Objects and Labor]], [[Work, output, and freedom]])
This is a result of *our economy*, this is **not** a result of a failure of our values. 


