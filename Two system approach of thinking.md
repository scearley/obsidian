There are two systems of thinking as outlined by Kahneman
# System 1 - Automatic
System 1 is instantaneous. Functionally instantaneous.It happens automatically, effortlessly, intuitively. It is based on a combination of instinct and experience.

This system of thinking is all of the fast automatic unconscious emotional brain responses to situations and stimuli. This includes input. The unaware symbol processing you do when you see words, for instance. 

It also includes the thoughtless things we do each day. Tying our shoes, instinctively avoiding puddles in the sidewalk.

This is also known as [[automatic thinking]].

# System 2 - Attentive
System 2 thinking is slower, and takes effort. It is conscious and logical.

This system of thinking is the slow, efforted, logical mode we are in when our brains operate solving more complicated problems. This includes looking for a friend in a crowd, parking in a small space, or memorizing an email address.

This is also known as [[Attentive Thinking]].

# Dual Process Model
In psychology, the dual process model is a combination of unconscious and conscious. It's not precisely the same as the Kahneman theory of two systems but it is close enough that "common" speech around psychology can use them interchangeably.

There is some issue that people think the automatic method of thought is maladaptive because it lacks logic. This isn't true. Automatic thinking is better when there is no time to deliberate, like slamming on the brakes to avoid a crash.

## Working together
The two processes work together. Automatic thinking and attentive thinking both are operating when we are awake. Automatic thinking runs in a constant state and attentive thinking is in at least a low-effort state.

Automatic thinking constantly generates suggestions for attentive thinking to act on:
- impressions
- feelings
- intentions

Attentive thinking accepts suggestions, turning them into beliefs and impulses into voluntary actions. When everything goes well, automatic thinking suggestions are accepted with little or no modification. This is obvious when you consider it - most of the time you believe your impressions and you act on your desires.

When automatic thinking has trouble, attentive thinking steps in to work on the situation and deliver a solution. It also happens when you are surprised or an event is detected that violates the model of the world that automatic thinking maintains.

## Working against each other

It's obvious how automatic thinking affects attentive thinking Attentive thinking can affect automatic thinking, to an extent, through conscious "reprogramming", such as developing different habits. Attentive thinking is responsible for self-control.
