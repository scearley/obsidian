This was proposed by Hermann Ebbinghaus in the late 19th century. 
It's little more than the statement that "people forget more as time goes on". In other words, the memory failure via transience. It doesn't cover any other issues that cause memory loss.

He did have a formula for it:
$$\large{b = \frac {100k}{log(t)^{c} + k}}$$
Where _b_ is how much is remembered, _t_ is minutes, _c_ is a constant of 1.25 and _k_ is a constant of 1.84. Various experiments after have shown this to be somewhat true.

The real contribution here is that Ebbinghaus invented artificial stimuli in psychological experimentation. His experiments used made-up syllables.

There is a lot of variation that isn't accounted for, such as how meaningful an input is to a person, but regardless memory does degrade and is added to or even supplanted by later learned information.

Is it possible this is related to the [[Federation (mind)]] theory of development? That as we learn and progress, that we forget things because a more efficient connection was found, leaving that memory space unaccessible?