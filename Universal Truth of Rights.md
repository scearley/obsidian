The only universal truth is a right to live. 
Until people believe this ***and*** act accordingly, all else is useless.

Society must share the means of existence. Only a bottom-up, localized, system of [[Federation]] serving the needs that must be fulfilled to maintain that right will end exploitation. 

cf [[Communist life is human history]]