Slogans such as *The right to work* and *To each the whole result of their labor* are not properly aligning causes and effects.

The people who make improvements or refinements to tools are not seen as having claim to fees or taxes upon those tools. They can't make any claims on the production or productivity of a person who uses them. (cf [[Inventions are not individual]])

In this same way, a "right to work" makes no claim on the humanity of the workers. It demands a labor structure that is deployed top-down, with the same management in authoritarian communism as it would have in capitalism. (cf [[Authoritarianism rising in socialism]])

No. There is no *right to work*. Humans have a ***right to well being***. A right to well being is the determination of one's own humanity. The "right to work" is the "right" to be contrained by servitude.

idea based on reading ch 1 and 2 of [[Conquest of Bread, The]]