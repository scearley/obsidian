People think "Oh, if it's like that, free food and shelter, I'm not gonna work, I'm going to play video games all day and take everything I need."
*This is American Thinking*
*This is only thinking about yourself*
## Hoarding
Consider all the hoarding that happened after the coronavirus. Or before a hurricane. Or for Y2K.
And if everything was free, these people would take 50 cars, 300 computers, and... for what? They're *free*. You can't resell them. 
Eventually people will have this reality ingrained within themselves and into their thinking process.
## Why work then?
"If everything's free, what's the point of working?" 
People want to work because it's fulfilling. The lazy person would be pitied. Why wouldn't you want to create? Why wouldn't you want to help the others? Why wouldn't you want to get up and do what you want?
Most people would want to work the basic tasks. What they do not want to do is feel alienated for doing them.
## What about the difficult jobs?
**People like doing hard work.**
But ok, there could be a job, or a number of jobs, that *nobody* wants to do. What then?
Well, the people would come together and figure out how to deal with it... or, figure out if it is truly necessary. And how many of those jobs would be eliminated? Enough so that we, our community, could work out a "chore chart" of sorts. And come together to really sort that out. We could all take these awful jobs in turn. And by the time it gets to the, say, CalTech grads to clean the sewer, they'd already have a robot constructed to do it for them.
## We can do this already, why don't we?
Because robots are used to generate profit, not to benefit humanity. Also: there are always poor people in a capitalist society who will be cheaper than robots. (cf [[Miserable jobs and exploitation]])