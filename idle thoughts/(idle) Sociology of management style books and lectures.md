It really seems like the books around "how to manage" - business books - change every half-generation or so in their philosophy. Ideas change, of course, but I wonder how much sociological tracking happens here.

Has there been a study? Are the books reactions to social mores as a whole, or are they driving them? That is, are they appearing on the market before the general concept or are they trailing it?

Is this related to [[(Idle) social expectations]]?