Cliches are not necessarily true or false. However, when they are true, they are only superficially true. 

Soes the superficiality mark context? The more superficial a statement is, the less context is necessary to understand it?

Are there non-superficial cliches?