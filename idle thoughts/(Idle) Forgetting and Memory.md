When people talk about memory loss, what's the time frame for this? If I can't remember something, then come up with it a few moments later, does that count as forgetting?

Has this been tested?

cf. [[Ebbinghaus's Forgetting Curve]]