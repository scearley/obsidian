When we want to do well, that means we are automatically thinking in terms of competition. Typically this means against others, or at least some part of society. It can sometimes mean competition with time, comparing current self versus old self. 
But mostly it's about others and the social norms expected of everyone.

We have this feeling of success, we want to feel success, but it is a learned behavior. We're taught that not doing something is wasteful. The social pressure is to work on and towards something greater than ourself. And in many cases that is internalized. In most cases that is using the scale of finance and monetary reward. Even in Open Source systems, where the reward is fame that presumes a level of respect and added opportunity, implying monetary reward. At the very least it confers an elite status that encourages a ruling elite hierarchy of coders. (cf. [[Culture of Gifting Objects and Labor]] and [[Arguments against lack of governmental structure (answered)]])

This feels related to [[(idle) Sociology of management style books and lectures]] but I'm not sure how beyond "management determines success parameters".

Idea from [[Idleness (ebook)]] in the introduction.

However, this competition is based solely on external demands. Competition is the result of opposing [[Mutual Aid]] efforts, of opposing the natural progression of communism that encourages growth. Comnpetition is not growth, it is sacrifice.