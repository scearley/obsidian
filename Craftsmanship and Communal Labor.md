A contrary notion to work freely being done for reputation is work being done for the passion, for the craftsmanship in that job.

In anarcho-communist circles, this kind of love for a specialized knowledge and skill that takes a lot of study and effort to attain, such as surgeon, or pharmaceutical researcher, and so forth, is the reward for taking that time and making that effort. The work is done because it *feels good to do the job to a high level*. (cf. [[Prestige and Work]])

To be able to measure quality, we must have people doing that work to create quality. Metrics, as it were, are constantly being created. What means something is beautifully made? Why would someone maximize quality of result over quantity of result?  After all, some work (cleaning, for example) requires a balanced level of both, and other work (wheat production maybe) requires quantity over quality, depending on the population size.

This is definitely a motivation to work, but [[Kropotkin]] seemed to frame it as the main motivation for the highly skilled/education intensive labor. It is more likely that the gift culture, with the metrics of peer evaluation, are the greater motivator. I think Kropotkin also acknowledges this as motivation but not as openly, and certainly not to the extent he frames it for the privilege of doing it for its own sake. (cf. [[Highly-Skilled Labor and Anarchism]])

In either case it fulfills a higher space of need than pure survival. One that is closer to self-actualization, self-love, and self-esteem. This would mean that this motivation would not arise until after the base needs are met, and thus could not be an immediate space for human potential in anarcho-communism, but only after the security of food has been met. (cf [[Revolutionary failures]], [[Money and Morality]])

(cf [[Adaptations of culture]] and [[Culture of Gifting Objects and Labor]])

partially from reading [[Eric Raymond]]'s "The Joy of Hacking" chapter in "Homesteading the Noosphere"'