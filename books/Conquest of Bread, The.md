# The Conquest of Bread
## Peter [[Kropotkin]]

ebook sourced from Project Gutenberg.

Kropotkin, Peter. _Conquest of Bread_ . New York, Vanguard Press, 1976. 

This ebook lists 183 pages.

an editorial remark about the book before the table of contents, presumably from the press, says that when this book is read in conjunction with Kropotkin's _Fields, Factories, and Workshops_ that it will answer the question "The Anarchist ideal is alluring, but how could you work it out?"

[Wikipedia article]([The Conquest of Bread - Wikipedia](https://en.wikipedia.org/wiki/The_Conquest_of_Bread))
