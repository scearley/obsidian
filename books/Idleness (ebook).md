A novella-length essay about the philosophy behind idleness, including moral thoughts and obligations.

ebook version is listed as 138 pages.

O'Connor, Brian. *Idleness. A Philosophical Essay*. Princeton University Press, 2018.