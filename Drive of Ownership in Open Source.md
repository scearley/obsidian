There are three basic ways of owning a piece of property:`
1. Homesteading (using labor and raw materials to create real property)
2. Transfer of Title (getting ownership from the previous owner)
3. Adverse possession of abandoned/derelict property. A variation of Homesteading

Based on Lockean theory, which is the formalization of common law of property rights from the thousand years before his codification, the impetus of Open Source programmers to own a project is to grow the value in the code. It must generate yield.

However, there is no yield in open source coding. There are accidental side-benefits, such as writing a book or becoming a teacher, but they are not from the direct improvement of the raw materials.

What is the drive to make all of this code for free, then? It's not economic.

cf [[Culture of Gifting Objects and Labor]] and [[Craftsmanship and Communal Labor]]

From [[Eric Raymond]]'s "Locke and Land Title" chapter of Homesteading the Noosphere.