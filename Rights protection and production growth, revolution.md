People's needs are protected, secured, and delivered on locally. This results, ultimately, in production going to the producer, and not the ruler.

Marx and Adam Smith both argue towards production ***and*** growth. [[Kropotkin]] argues that basic needs must be met and secured in any revolution (cf [[Freedom Movements]]).

A revolution is not judged on some abstract quantity or quality of material production.