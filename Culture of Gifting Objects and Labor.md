Most of the social world we live in right now is an exchange economy. It is decentralized ([[Federation]]), but is also based on hoarding. It is an adaptation to scarcity that scales, unlike the proto-caveman Chieftain structure.

Abundance makes the Chieftain structure impossible to maintain, and it makes exchange relationships pointless, which requires those relationships to create false scarcity.

In open source, and thus to some extent in [[Mutual Aid]], there is a gift culture. The gift culture is an adaptation to abundance rather than one to scarcity. (In mutual aid, for instance, there is plenty available, but there is no method of distribution, and mutual corrects this abundance situation) Other cultures with gift culture such as the Kwakiutl and their potlatch, use the gifts as a method of showing status. Similarly, billionaires donate in order to spread their name around and show how big they are by tossing all that money at libraries and hospitals and so forth.

With open source, the programmer users are using their work (or ownership, cf [[Drive of Ownership in Open Source]]) on free software to generate a yield not in terms of real returns but in terms of status within that community. Reputation is the only measure of competition and success.

Warez crackers use the distribution of cracks for the same ends, but with the addition of a scarcity layer of the education on how to use tools for cracking. The tools (and warez) are freely available, but the skill and ability is shrouded and hidden away.

cf [[Eric Raymond]], "The Hacker Milieu as GIft Culture" in "Homesteading the Noosphere"