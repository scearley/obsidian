Federation occurs in software development to prevent forking code. Forks end up creating competing methods of interaction, either intentionally or unintentionally, but the fork itself is a split in the developer community. The implications of this towards Federation in general is obvious.

## Forking
Forking is one method of risking the destruction of federation. The advantage of any federation is the unified goal and notion that local control is not given up while you may still have disparate groups; each group then acts as a person in a metademocracy that is the federation.

Forking in software refers to a group leaving the whole to form a new community. A new fork in development means typically changing the procedures within the software development without starting from scratch. It also means the losses implied to the original group are smaller, since a copy is taken, and not the entirety. (cf. [[Inventions are not individual]])

## Forking "IRL"
This type of forking often happens in [[Mutual Aid]]  groups, religions ([[Hypocrisy in church leadership]]), and non-profit organizations. A subset of the people walk away and start a new group, with different but probably similar goals. They take with them the connections, the non-physical portions of mutual aid. They also likely destroy the connection they had with the earlier group, though this does not have to be the case.

[[Open Source Codebase growth as Mutual Aid]]