A federation is a collection of groups that agree upon standards of operation collectively.
The groups themselves do not have to be run collectively, but the mutualism between groups is necessary. There is no state ownership or outside demands.

[[Federation (mutual aid)]]

[[Federation (mind)]]

[[Federation (software)]]

