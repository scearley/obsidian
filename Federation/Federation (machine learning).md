Federated learning is a method of using algorithms across multiple, decentralized collections of data, and not exchanging or combining that data. 

Data is not stored in one location, on one server, or anything like that. Each location is maintained autonomously, and allows for security and privacy while sharing that data.

Realistically, this also means that the distribution is not equal. Larger stores and smaller stores all contribute with relative equality, but are not necessarily valued more highly than others. This also means that the way each node/group communicates with another is not dictated by a centralized state but rather by the needs/requirements of the communal operation.

cf. [[Federation (mind)]] and [[Mutual Aid]]