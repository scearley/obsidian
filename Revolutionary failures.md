Previous revolutions failed because they could not ensure needs were met. Mainly because the people were going hungry.

Revolutions in the 17th (??), 18th, 19th centuries were fought and in doing so destroyed ways to produce food and to transport the food. (cf [[Communist life is human history]]) 

However, the revolutions continued to define the duties of the proletariat. (cf [[Authoritarianism rising in socialism]]) The needs must be fulfilled from the very first day. You can't merely give slogans. There must be word AND action for caring for the people. Once that happens, once there is a security of needs being met, *then* the demands of the group upon the individual can be taught.

Hunger, especially, is the issue. People would rather have bad governance than die of starvation.

idea from ch 2 of [[Conquest of Bread, The]]
