Any lack in the success of meeting the needs - not surpassing, only meeting - of the people means that something is __fundamentally__ wrong. This broken foundation is the basis for capitalism. 

When there is not enough to meet the needs of people to feed themselves, then the revolution devolves into a justification of ranking needs based on merit. And this only solidifies the positions of those revolutionaries who "won".

(cf. [[Hypocrisy in church leadership]], [[Revolutionary failures]], [[Universal Truth of Rights]])