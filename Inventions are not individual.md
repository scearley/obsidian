There is no such thing as the individualist. No person is capable of making all of their own tools. They also have to get their knowledge of bushcraft from somewhere else. It's not innate.

In the same way, there are no true inventions.

Thousands of hours of unnamed labor are required to get society as a whole to progress to the point of any invention. The conveyor belt could not have been "invented" in the 12th century. Microscopes could not have been invented in Ancient Greece.

Not only that, there are all of the person-hours involved, numbers of people working numbers of hours, to refine an invention to usability. To take the invention and use it and make it better, better to the point where it actually does the stated job.

And with inventions, the invention itself will require hours of maintenance and hours of persons maintaining it. This is where craftsmanship arises, and where the advantage of hyperskilled labor (e.g. surgeon) will still exist in the anarcho-communist world.

The time spent in being a craftsman, the love in doing that thing day after day after day, that social reward is the ability to continue with that focus to be able to attain that exceptional level of required skill and knowledge.

There is no possible way one person can lay claim to an invention and reap sole reward of it. 

Similarly, there is no possible way one person cannot be responsible for a revolution (cf [[Authoritarianism rising in socialism]]) one person can be responsible for a revolution.

idea originated in [[Conquest of Bread, The]], refined in "The Joy of Hacking" in "Homesteading in the Noosphere" by [[Eric Raymond]]


