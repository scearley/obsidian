Culture adapts based on two things:
1. conscious ideology, and
2. implicit/semi- (or un-) conscious knowledge

It is common that the implicit knowledge and conscious ideology are regularly contradictory.