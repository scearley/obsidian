## Writing

Writing is the honest manifestation of alchemy. It is a lone pursuit, but must be learned with consultation before becoming a lone pursuit.

Writing is honest and real. Unlike alchemy. But it is about getting to that honesty and reality. 
This is true of the story writers. But is it true of the technical writers?

## Technical Writing
The function of alchemy aligns with technical writing. It can change worlds. Just as alchemy will affect lives, technical writing will affect lives. And must be treated as such.