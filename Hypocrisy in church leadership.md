Teachings of the church are all about how the rich are the same as the poor, and that the rich _must_ share everything with the poor or they will burn in eternity.

More generally, that those who have must make available to those who have not.

Yet the preachers, that is, the _leaders_ of the church and controllers of its land and related valuables, state that such teachings are merely parables, not to be taken as literal truth.

This is the same as the issues had with leadership of modern socialism (cf [[Authoritarianism rising in socialism]])

idea from ch 1 in [[Conquest of Bread, The]]