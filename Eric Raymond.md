Author of *The Cathedral and the Bazaar* and *Homesteading the Noosphere*. Both of these works are about the anthropology and sociology of Open Source and its participants.

Cathedral and the Bazaar - [The Cathedral and the Bazaar | Satoshi Nakamoto Institute](https://nakamotoinstitute.org/the-cathedral-and-the-bazaar/)

Homesteading the Noosphere - [Homesteading the Noosphere (catb.org)](http://catb.org/~esr/writings/homesteading/homesteading/index.html)