If prestige were different in different jobs, people would have different jobs.
In capitalist societies, prestige is 95% tied to the amount of money made.
(I say 95% because there are a few jobs that pay less but offer more social clout, which is affected by the overarching pay scales.)

Who objects to equal - meaning zero - prestige based on skill, or on salary?

People who object to everyone having the same basic needs fulfilled are the people who have no special skill, despite the promises of the framework of capitalism.

>"I went through all that schooling so I should get paid a lot."

That is because these people do not have the interest nor the special skill.

When you get a skill, using it is its own reward. (cf. [[Highly-Skilled Labor and Anarchism]])